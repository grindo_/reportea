import os
import xlsxwriter
import logging
from datetime import datetime
from datetime import timedelta


logging.basicConfig(level=logging.DEBUG, 
                          format='%(asctime)s %(levelname)s: %(message)s', 
                          handlers=[logging.StreamHandler()]
                          )


def make_xlsx(list, report_dir='reports'):
    xlsx_filename = os.path.join(report_dir, f'{datetime.now().strftime("%d%m%Y_%H%M%S")}.xlsx')
    logging.debug(f'Preparing xlsx file with name: {xlsx_filename}')

    with xlsxwriter.Workbook(xlsx_filename) as wbook:
        wsheet = wbook.add_worksheet()
        wsheet.set_column(0, 0, 40)
        wsheet.set_column(1, 1, 60)
        wsheet.set_column(2, 2, 40)

        format_expiring = wbook.add_format({'bold': True, 'bg_color': '#ffe994'})
        format_severely_expiring = wbook.add_format({'bold': True, 'bg_color': '#ec9ba4'})
        format_toasted = wbook.add_format({'bold': True, 'font_color': '#FFFFFF', 
                                           'bg_color': '#761000'})

        xlsx_row = 0
        xlsx_col = 0

        for c in list:
            cell_format = wbook.add_format({'bold': False})

            #logging.debug(datetime.now() + timedelta(days=14))
            #logging.debug(c.stopdate - timedelta(days=14))
            if c.stopdate < datetime.now():
                cell_format = format_toasted
                logging.debug('Toasted certificate found')
            elif c.stopdate - timedelta(days=14) < datetime.now():
                cell_format = format_severely_expiring
                logging.debug('Severely expiring certificate found')
            elif c.stopdate - timedelta(days=30) < datetime.now():
                logging.debug('Expiring certificate found.')
                cell_format = format_expiring

            row = [(c.name, c.serial)]
            #logging.debug(row)
            for name, serial in (row):
                logging.debug(f'Writing to report file: {name}')
                logging.debug(f'Writing to report file: {serial}')
                wsheet.write(xlsx_row, xlsx_col, name, cell_format)
                wsheet.write(xlsx_row, xlsx_col + 1, serial, cell_format)
                wsheet.write(xlsx_row, xlsx_col + 2, c.stopdate.strftime('%d.%m.%Y'), cell_format)
                xlsx_row += 1

    logging.debug(f'Report {xlsx_filename} complete.')
