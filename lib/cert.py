#!/usr/bin/python3

import re
import os
import json
import logging
import subprocess
from pathlib import Path, WindowsPath
from datetime import datetime, timedelta

logging.basicConfig(level=logging.DEBUG, 
                          format='%(asctime)s %(levelname)s: %(message)s', 
                          handlers=[logging.StreamHandler()]
                          )

#with open('settings.json') as s:
#    settings = json.load(s)

class Cert:
    '''Parent Cert class'''

    def __init__(self, serial, org, name, position, startdate, stopdate, snils):
        self.serial = serial
        self.org = org
        self.name = name
        self.position = position
        self.startdate = datetime.strptime(startdate, '%d/%m/%Y  %H:%M:%S %Z')
        self.stopdate = datetime.strptime(stopdate, '%d/%m/%Y  %H:%M:%S %Z')
        self.snils = snils
    
    @classmethod
    def from_certmgr_output(cls, file, certmgr_bin):
        '''Example docstring'''
        stream = ''
        try:
            logging.debug('Trying os.uname()')
            os.uname()
            file_path = os.path.join(file)
            stream = subprocess.check_output([certmgr_bin, '-list', '-file', f'{file_path}'], shell=False)
            string = stream.decode('utf-8')
        except AttributeError:
            file_path = WindowsPath(file)
            stream = subprocess.check_output(['certmgr.exe', '-list', '-file', f'{file_path}'], shell=True)
            string = stream.decode('cp866', errors='replace')
        cert_bin = os.path.join(certmgr_bin)

        name = ''

        list = string.split('\n')
        for line in list:
            #logging.debug(line)
            if line.startswith('Subject'):
                    logging.debug(f'Found Subject section')
                    try:
                        name = re.search(r'( SN=)(.+?)(\, [A-Za-z]+=)', line).group(2)
                        match = re.search(r'( G=)(.+?)(\, [A-Za-z]+=)', line)
                        name += ' ' + match.group(2)
                    except:
                        name = 'None'
                    try:
                        org = re.search(r'( O=)(.+?)(, [A-Z]+=)', line).group(2)
                        if org.startswith('"'):
                            org = org[1:-1]
                        org = org.replace('""', '"')
                    except:
                        org = name
                    try:
                        snils = re.search(r'(SNILS=)(.+?)(\, [A-Z]+=)', line).group(2)
                    except:
                        snils = None
                    try: 
                        position = re.search(r'( T=)(.+?)(\, [A-Z]+?=)', line)
                        position = position.group(2)
                    except:
                        position = None
                    logging.debug(f'Found in subject section: name = {name}, org = {org},'\
                            'snils = {snils}, position = {position}')
                
            if line.startswith('Serial'):
                logging.debug(f'Found Serial section')
                sn = line.split(' ')[-1][2:]
                #sn = re.search(r'(0x)(.+?)(\n+)',  line).group(2)
                logging.debug(f'Serial: {sn}')
            if line.startswith('Not valid before'):
                logging.debug(f'Found Start date section')
                startdate = line.split(' : ')[1]
                startdate = startdate.strip()
                #startdate = datetime.strptime(startdate, '%d/%m/%Y %H:%M:%S %Z')
                logging.debug(f'Start date: {startdate}')
            if line.startswith('Not valid after'):
                logging.debug(f'Found Stop date section')
                stopdate = line.split(' : ')[1]
                stopdate = stopdate.strip()
                #stopdate = datetime.strptime(stopdate, '%d/%m/%Y %H:%M:%S %Z')
                logging.debug(f'Stop date: {stopdate}')

        return cls(sn, org, name, position, startdate, stopdate, snils) 

    def is_valid_today(self):
        if (self.stopdate - datetime.today()).days <= 0:
            return False
        else:
            return True

    def is_expiring(self):
        if (self.stopdate - datetime.today()).days <= 14:
            return True
        else:
            return False

class OrgCert(Cert):
    
    def __init__(self, serial, org, name, position, startdate, stopdate, snils, ogrn=None):
        super().__init__(serial, org, name, position, startdate, stopdate, snils)
        self.ogrn = ogrn

    @classmethod
    def from_certmgr_output(cls, file):
        cert_bin = settings['certmgr_bin']

        stream = os.popen(cert_bin + ' -list -file "' + os.path.abspath(file) + '"')
        string = stream.read()

        name = ''

        list = string.split('\n')
        for line in list:
            if line.startswith('Subject'):
                    name = re.search(r'(SN=)(.+?)(\, [A-Z]+=)', line).group(2)
                    match = re.search(r'(G=)(.+?)(\, [A-Z]+=)', line)
                    name += ' ' + match.group(2)
                    org = re.search(r'(O=")(.+?)("\, [A-Z]+=)', line).group(2)
                    org = org.replace('""', '"')
                    snils = re.search(r'(SNILS=)(.+?)(\, [A-Z]+=)', line).group(2)
                    position = re.search(r'(T=)(.+?)(\, [A-Z]+=)', line).group(2)
                    ogrn = re.search(r'(OGRN=)(.+?)(\, [A-Z]+=)', line).group(2)

            if line.startswith('Serial'):
                sn = line.split(' ')[-1][2:]
            if line.startswith('Not valid before'):
                startdate = line.split(' : ')[1]
            if line.startswith('Not valid after'):
                stopdate = line.split(' : ')[1]

        return cls(sn, org, name, position, startdate, stopdate, snils, ogrn) 

class Org:

    def __init__(self, name, ogrn=None, inn=None, authority=None):
        self.name = name
        if ogrn is not None:
            self.ogrn = ogrn
        else:
            self.ogrn = None
        if inn is not None:
            self.inn = inn
        if authority is not None:
            self.authority = authority

