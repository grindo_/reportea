import os
import pytz
import json
import logging
import tkinter as tk
from tkinter import ttk
from datetime import datetime, timedelta
from tkinter import filedialog
from tkinter.messagebox import showinfo

import xlsxwriter

from lib.cert import Cert
from lib.report import make_xlsx


logging.basicConfig(level=logging.DEBUG, 
                          format='%(asctime)s %(levelname)s: %(message)s', 
                          handlers=[logging.StreamHandler()]
                          )

REPORT_DIR = os.path.abspath('reports')
cert_list = []

with open('settings.json') as s:
    settings = json.load(s)

def load_certdir(dir):
    global cert_list

    for file in os.scandir(dir):
        logging.debug(f'Looking at file {file}')
        cert = Cert.from_certmgr_output(file, settings['certmgr_bin'])
        cert_list.append(cert)
        #logging.debug(f'{cert.serial} {cert.name}')

#def make_xlsx(list):
#    xlsx_filename = os.path.join(REPORT_DIR, f'{datetime.now().strftime("%d%m%Y_%H%M%S")}.xlsx')
#    with xlsxwriter.Workbook(xlsx_filename) as wbook:
#        wsheet = wbook.add_worksheet()
#        wsheet.set_column(0, 0, 40)
#        wsheet.set_column(1, 1, 80)
#
#        format_expiring = wbook.add_format({'bold': True, 'bg_color': '#ffe994'})
#        format_severely_expiring = wbook.add_format({'bg_color': '#ec9ba4'})
#        format_toasted = wbook.add_format({'bg_color': '#761000'})
#
#        xlsx_row = 0
#        xlsx_col = 0
#
#        for c in list:
#            cell_format = wbook.add_format({'bold': False})
#
#            logging.debug(datetime.now() + timedelta(days=14))
#            logging.debug(c.stopdate - timedelta(days=14))
#            if c.stopdate < datetime.now():
#                cell_format = format_toasted
#            elif c.stopdate - timedelta(days=14) < datetime.now():
#                cell_format = format_severely_expiring
#            elif c.stopdate - timedelta(days=30) < datetime.now():
#                logging.debug('Expiring certificate found.')
#                cell_format = format_expiring
#
#            row = [(c.name, c.serial)]
#            logging.debug(row)
#            for name, serial in (row):
#                logging.debug(name)
#                logging.debug(serial)
#                wsheet.write(xlsx_row, xlsx_col, name, cell_format)
#                wsheet.write(xlsx_row, xlsx_col + 1, serial, cell_format)
#                xlsx_row += 1

#def pb_window_open():
#    top = tk.Toplevel(root)
#    top.geometry('400x150')
#    top.title('Processing')
#    pb = ttk.Progressbar(
#            top,
#            orient='horizontal',
#            mode='determinate',
#            length='380'
#            )
#    pb.pack()

def button_cmd():
    global cert_list

    #top = tk.Toplevel(root)
    #top.geometry('400x50')
    #top.title('Processing')
    #tk.Label(top, text='Думою').pack()
    #pb = ttk.Progressbar(
    #        top,
    #        orient='horizontal',
    #        mode='indeterminate',
    #        length='380'
    #        )
    #pb.pack()
    #pb.start() 

    #top.after(1, lambda: load_certdir(WORK_DIR.get()))
    #top.after(1, lambda: make_xlsx(cert_list))
    #top.after(1000, top.destroy)
    load_certdir(WORK_DIR.get())
    make_xlsx(cert_list)
    cert_list = []
    showinfo(message='Complete!')

def choose_dir():
    global WORK_DIR

    workdir = filedialog.askdirectory()
    WORK_DIR.set(workdir)



root = tk.Tk()

root.geometry('450x140')
root.resizable(False, False)
root.title('Смотритель сертов 4000')

WORK_DIR = tk.StringVar()
WORK_DIR.set(os.path.abspath('cert'))

# Render interface grind
l1 = tk.Label(root, text='Где серты лежат:')
l1.grid(row=0, column=0, columnspan=5, padx=10, pady=5, sticky='w')

pathlabel = tk.Label(root, textvariable=WORK_DIR, relief=tk.SUNKEN)
pathlabel.grid(row=1, column=0, columnspan=4, padx=10, pady=5, sticky='we')

dirbutton = tk.Button(root, text='Обзор', command=choose_dir)
dirbutton.grid(row=1, column=5, padx =10, pady=5, sticky='e')

button = tk.Button(root, text='Погнали', command=button_cmd)
button.grid(row=2, column=0, padx=10, pady=10, sticky='w')

qbutton = tk.Button(root, text='Выйти', command=root.destroy)
qbutton.grid(row=2, column=5, padx=10, pady=10, sticky='e')
# End of rendering interface


if __name__ == '__main__':
    root.mainloop()
